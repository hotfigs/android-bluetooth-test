package me.geoff.android.bluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class BluetoothTest extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            handleRequestEnableBt(responseCode);
        }
    }

    public void toggle(View view) {
        //Intent intent = new Intent(this, ToggleMessageActivity.class);
        View v = findViewById(R.id.counter);
        TextView textBox = (TextView) v;
        counter++;
        textBox.setText(Integer.toString(counter));
    }

    public void sendBluetooth(View view) {
        sendBluetoothMessage();
    }

    private void handleRequestEnableBt(int responseCode) {
        if (responseCode == RESULT_CANCELED) {
            Toast.makeText(this, "Could not enable Bluetooth", Toast.LENGTH_SHORT).show();
        } else if (responseCode == RESULT_OK) {
            sendBluetoothMessage();
        }
    }

    private void sendBluetoothMessage() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            Toast.makeText(this, "Bluetooth adapter not available", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!adapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        BluetoothDevice chronos = findBluetoothDevice("CHRONOS", adapter);
        if (chronos == null) {
            Toast.makeText(this, "Is not paired with Chronos", Toast.LENGTH_SHORT).show();
            return;
        }
        chronos.
    }

    private BluetoothDevice findBluetoothDevice(String name, BluetoothAdapter adapter) {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices != null) {
            for (BluetoothDevice device : pairedDevices) {
                Toast.makeText(this, "Found device: " + device.getName(), Toast.LENGTH_SHORT).show();
            }
            if (pairedDevices.size() == 0) {
                Toast.makeText(this, "No paired devices", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private int counter = 0;
    private static final int REQUEST_ENABLE_BT = 1;
}
